""" Classes to serialize models relating to Suppliers """


from rest_framework import serializers

from .models import SecretText


class SecretTextSerializer(serializers.ModelSerializer):
    """ Serializer for Secret Text """

    secret_text = serializers.CharField()

    class Meta:
        model = SecretText
        fields = [
            "id",
            "secret_text"
        ]

    def create(self, validated_data):
        validated_data.pop('secret_text')
        obj = SecretText.objects.create(**validated_data)
        obj.save()

        return obj
