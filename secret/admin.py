from django.contrib import admin

from .models import SecretText


class SecretTextAdmin(admin.ModelAdmin):
    """ Admin page for the SecretText model. """

    list_display = ("user",)
    search_fields = ["user"]
    list_filter = ["user"]

admin.site.register(SecretText, SecretTextAdmin)
