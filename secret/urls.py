""" URL configuration for the Organisation app. """
from rest_framework import routers

from . import views

app_name = "secret"

router = routers.SimpleRouter()
router.register("", views.SecretTextViewSet, basename="secrettext")
urlpatterns = router.urls
