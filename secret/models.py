"""models.py: Models relating to the storage of Secret Text. """

from django.db import models
from django.conf import settings


class SecretText(models.Model):
    """ Model depicting secret text. """

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             help_text='User who the secret text belongs to',
                             on_delete=models.CASCADE)
    file_path = models.FileField(help_text="Path of the file that contains the secret text")

    @property
    def secret_text(self):
        """ Read the secret text from the file it is contained in. """

        return self.file_path.read().decode('utf-8')
    