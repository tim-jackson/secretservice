""" Views for Creating and Retrieving secret text. """

import logging
import uuid

from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated

from .models import SecretText
from .serializers import SecretTextSerializer

LOGGER = logging.getLogger(__name__)


class SecretTextViewSet(viewsets.ModelViewSet):
    """ Restrictive view set for Secret text - cannot create or delete via the API """

    serializer_class = SecretTextSerializer

    def perform_create(self, serializer):
        """ Create a file containing the secret text, and add the file path to the
            serializer for storage in the database. """

        path = default_storage.save(f'{settings.SECRET_FILES_ROOT}/{self.request.user.id}/' + str(uuid.uuid4()),
                                    ContentFile(serializer.validated_data.get('secret_text')))
        text_object = serializer.save(user=self.request.user,
                                      file_path=path)
        LOGGER.info("User %s created secret text with ID %s", self.request.user, text_object.id)


    def perform_update(self, serializer):
        """ Delete the existing secret text, and add the new secret text. """

        file_path = serializer.instance.file_path.name
        default_storage.delete(serializer.instance.file_path.name)
        default_storage.save(file_path, ContentFile(serializer.validated_data.get('secret_text')))

        LOGGER.info("User %s edited secret text with ID %s",
                    self.request.user, serializer.instance.id)

    def get_queryset(self):
        LOGGER.info("User %s is retrieving their secret text", self.request.user)
        return SecretText.objects.filter(user=self.request.user).all().order_by('-id')
