# Secret Service
Secret Service stores text on a per-user basis. This is the API component of Secret Service, implemented in Python 3 and Django 2.2.

# Installation
To set-up an instance that runs locally, the following commands should be run:
```sh
$ virtualenv venv -p python3
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

To create an initial user of the Secret Service, run the following command:
```sh
$ python manage.py createsuperuser
```
